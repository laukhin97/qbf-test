import socket
import time
import json

IP = 'localhost'
PORT = 8080
BUFFER_SIZE = 1024
RECONNECT_PERIOD = 1


class ClientSocket:
    def __init__(self, ip, port, buffer_size):
        self._ip = ip
        self._port = port
        self._buffer_size = buffer_size
        self._socket = None

    def connect(self):
        self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        while True:
            print('Trying to connect')
            try:
                self._socket.connect((IP, PORT))
                break
            except ConnectionRefusedError:
                print('Connection failed')
                time.sleep(RECONNECT_PERIOD)
                continue

    def receive_data(self):
        return self._socket.recv(BUFFER_SIZE)

    def __enter__(self):
        self.connect()
        return self

    def __exit__(self, *args):
        self._socket.close()


if __name__ == '__main__':
    with ClientSocket(IP, PORT, BUFFER_SIZE) as client_socket:
        while True:
            raw_message = client_socket.receive_data()
            if not raw_message:
                client_socket.connect()

            try:
                data = json.loads(raw_message.decode('utf-8'))
            except json.JSONDecodeError:
                continue

            print(str.format("Ticker: {0}", data['Ticker']))
            print(str.format("Price: {0}", data['Price']))
