package main

import (
	"encoding/json"
	"log"
	"math/rand"
	"net"
	"time"
)

const letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
const period = 1*time.Second
const maxTickerLength = 5
const minTickerLength = 3

type message struct {
	Ticker string
	Price float32
}

func generateRandomTicker() string {
	numLetters := rand.Intn(maxTickerLength - minTickerLength) + minTickerLength
	ticker := make([]byte, numLetters)
	for i := range ticker {
		ticker[i] = letters[rand.Intn(len(letters))]
	}
	return string(ticker)
}

func generateRandomPrice() float32 {
	return 10000*rand.Float32()
}

func handleClientConnection(conn net.Conn)  {
	for {
		time.Sleep(period)
		rand.Seed(time.Now().Unix())
		message := message{Ticker: generateRandomTicker(), Price: generateRandomPrice()}
		jsonMessage, _ := json.Marshal(message)
		conn.Write(jsonMessage)
	}
}

func main() {
	ln, err := net.Listen("tcp", ":8080")
	if err != nil {
		log.Fatal("Error while creating a listener")
	}
	defer ln.Close()
	for {
		conn, err := ln.Accept()
		if err != nil {
			log.Print("Error while connecting to the client")
		}
		go handleClientConnection(conn)
	}
}
